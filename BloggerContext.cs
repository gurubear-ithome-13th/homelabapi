using Microsoft.EntityFrameworkCore;
namespace homelabAPI
{
    public class BloggerContext : DbContext
    {
        public  BloggerContext(DbContextOptions<BloggerContext> options): base(options)
        {

        }

        public  DbSet<Posts> Posts {get; set;}
        public  DbSet<Users> Users {get; set;}

    }
}
