using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace homelabAPI
{
    public class PostDTO
    {
        public String Title { get; set; }
        public String Content { get; set; }

        [MaxLength(50)]
        public String Tag { get; set; }

        [Required]
        public Guid UserID { get; set; }
    }
}
