FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /source
COPY . .
RUN dotnet publish -c release -o /app 

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app ./
USER 1001
ENV TZ=Asia/Taipei
ENV ASPNETCORE_URLS=http://+:7777
ENTRYPOINT ["dotnet", "homelabAPI.dll"]