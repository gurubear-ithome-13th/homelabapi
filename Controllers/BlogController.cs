using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;

namespace homelabAPI.Controllers
{
    [ApiController]
    [EnableCors("Policy1")]
    public class BlogController : ControllerBase
    {
        private readonly ILogger<BlogController> _logger;
        private readonly BloggerContext _context;

        public BlogController(ILogger<BlogController> logger,BloggerContext context)
        {
            _logger = logger;
            _context = context;
        }
        [Route("API/[controller]/[action]")]
        [HttpGet]
        public  List<PostListVM> GetList(string tag=null)
        {
            var itemlist = _context.Posts;
            if(tag==null)
                return _context.Posts.OrderByDescending(u => u.ModifyTime).Select(o=>new PostListVM{
                    PostID = o.PostID,
                    Title = o.Title,
                    Tag = o.Tag,
                    CreateTime = o.CreateTime,
                    ModifyTime = o.ModifyTime
                }).ToList();
            else
                return _context.Posts.Where(p=>p.Tag.Contains(tag)).OrderByDescending(u => u.ModifyTime).Select(o=>new PostListVM{
                    PostID = o.PostID,
                    Title = o.Title,
                    Tag = o.Tag,
                    CreateTime = o.CreateTime,
                    ModifyTime = o.ModifyTime
                }).ToList();
        }
        [Route("API/[controller]/[action]")]
        [HttpGet]
        public  PostVM GetDetail(Guid ID)
        {
            return _context.Posts.Include(o => o.Users).Where(q => q.PostID ==ID).Select(p=> new PostVM {
                PostID = p.PostID,
                Title = p.Title,
                Content = p.Content,
                Tag = p.Tag,
                CreateTime = p.CreateTime,
                ModifyTime = p.ModifyTime,
                Author = p.Users.Name
            }).FirstOrDefault();
        }

        [HttpPost]
        [Route("Internal/[controller]/[action]")]
        public async Task<bool> AddUser(string Name)
        {
            Users item = new Users {
                Name = Name,
                CreateTime = DateTime.Now,
                ModifyTime = DateTime.Now
            };
            var result =  await _context.Users.AddAsync(item);
            _context.SaveChanges();
            return true;
        }

        [HttpGet]
        [Route("Internal/[controller]/[action]")]
        public List <Users> GetUsers()
        {
            return _context.Users.ToList();
        }

        [HttpPost]
        [Route("Internal/[controller]/[action]")]
        public async Task<bool> AddPost(PostDTO item)
        {
            Posts entity = new Posts{
                Title = item.Title,
                Content = item.Content,
                Tag = item.Tag,
                CreateTime = DateTime.Now,
                ModifyTime = DateTime.Now,
                UserID = item.UserID
            };
            var result = await _context.Posts.AddAsync(entity);
            _context.SaveChanges();
            return true;
        }
    }
}
