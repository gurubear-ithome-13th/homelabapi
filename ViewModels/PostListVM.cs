using System;
using System.ComponentModel.DataAnnotations;

namespace homelabAPI
{
    public class PostListVM
    {
        public Guid PostID { get; set; }
        [MaxLength(50)]
        public String Title { get; set; }

        [MaxLength(50)]
        public String Tag { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ModifyTime { get; set; }

    }
}
