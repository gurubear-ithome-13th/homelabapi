using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace homelabAPI
{
    public class Users
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserID { get; set; }
        [MaxLength(20)]
        public String Name { get; set; }
        [Required]
        public DateTime CreateTime { get; set; }
        [Required]
        public DateTime ModifyTime { get; set; }

    }
}
