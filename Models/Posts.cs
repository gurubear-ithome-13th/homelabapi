using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace homelabAPI
{
    public class Posts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PostID { get; set; }
        [MaxLength(50)]
        [Required]
        public String Title { get; set; }

        public String Content { get; set; }

        [MaxLength(50)]
        public String Tag { get; set; }
        [Required]
        public DateTime CreateTime { get; set; }
        [Required]
        public DateTime ModifyTime { get; set; }
        
        [Required]
        public Guid UserID { get; set; }

        [ForeignKey("UserID")]
        public Users Users {get; set;}
    }
}
